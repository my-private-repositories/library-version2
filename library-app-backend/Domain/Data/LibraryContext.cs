using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

using library_app_backend.Domain.Models;

namespace library_app_backend.Domain.Data
{
    public class LibraryContext : DbContext
    {
        private DbSet<User> _users;
        private DbSet<UserCredential> _usersCredentials;
        private DbSet<Book> _books;
        private DbSet<BookGenre> _booksGenres;
        private DbSet<Author> _authors;

        public LibraryContext(DbContextOptions<LibraryContext> options) : base(options)
        {
        }

        public DbSet<User> Users 
        { 
            get { return _users; } 
            set { _users = value; } 
        }

        public DbSet<UserCredential> UsersCredentials 
        { 
            get { return _usersCredentials; } 
            set { _usersCredentials = value; } 
        }

        public DbSet<Author> Authors 
        { 
            get { return _authors; } 
            set { _authors = value; } 
        }

        public DbSet<BookGenre> BooksGenres 
        { 
            get { return _booksGenres; } 
            set { _booksGenres = value; } 
        }

        public DbSet<Book> Books 
        { 
            get { return _books; } 
            set { _books = value; } 
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<User> (
                us =>
                {
                    us.OwnsOne(
                        u => u.Credential,
                        us =>
                        {
                            us
                                .Property(us => us.UserRole)
                                .HasConversion<string>();
                            us
                                .HasIndex(us => us.UserName)
                                .IsUnique();
                        });
                });
            builder.Entity<User> ()
                .HasIndex(u => u.Email)
                .IsUnique();
            
            builder.Entity<Author> ()
                .HasMany<Book>(a => a.Books)
                .WithMany(b => b.Authors)
                .UsingEntity<Dictionary<string, object>> (
                    "books_authors",
                    j => j
                        .HasOne<Book>()
                        .WithMany()
                        .HasForeignKey("book_id")
                        .HasConstraintName("FK_books_authors_book_id")
                        .OnDelete(DeleteBehavior.Cascade),
                    j => j
                        .HasOne<Author>()
                        .WithMany()
                        .HasForeignKey("author_id")
                        .HasConstraintName("FK_books_authors_author_id")
                        .OnDelete(DeleteBehavior.Cascade));

            builder.Entity<Book> ()
                .HasMany<BookGenre>(a => a.BookGenres)
                .WithMany(b => b.Books)
                .UsingEntity<Dictionary<string, object>> (
                    "books_genres",
                    j => j
                        .HasOne<BookGenre>()
                        .WithMany()
                        .HasForeignKey("genre_id")
                        .HasConstraintName("FK_books_genres_genre_id")
                        .OnDelete(DeleteBehavior.Cascade),
                    j => j
                        .HasOne<Book>()
                        .WithMany()
                        .HasForeignKey("book_id")
                        .HasConstraintName("FK_books_genres_book_id")
                        .OnDelete(DeleteBehavior.Cascade));

            builder.Entity<Book>()
                .Property(b => b.Price)
                .HasPrecision(10, 2);
        }
    }
}