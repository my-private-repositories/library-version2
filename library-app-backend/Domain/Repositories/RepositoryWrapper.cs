using library_app_backend.Domain.Data;

namespace library_app_backend.Domain.Repositories
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private LibraryContext _context;
        private IUsersRepository _usersRepo;

        public RepositoryWrapper(LibraryContext ctx) 
        {
            _context = ctx;
        }

        public IUsersRepository UsersRepository
        { 
            get 
            {
                if(_usersRepo == null)
                    _usersRepo = new UsersRepository(_context);

                return _usersRepo;
            } 
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}