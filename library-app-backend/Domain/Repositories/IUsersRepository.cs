using library_app_backend.Domain.Models;

namespace library_app_backend.Domain.Repositories
{
    public interface IUsersRepository: IRepositoryBase<User>
    {

    }
}