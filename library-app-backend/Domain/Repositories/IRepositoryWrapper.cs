namespace library_app_backend.Domain.Repositories
{
    public interface IRepositoryWrapper
    {
        IUsersRepository UsersRepository { get; }
        void Save();
    }
}