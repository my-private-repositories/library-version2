using library_app_backend.Domain.Data;
using library_app_backend.Domain.Models;

namespace library_app_backend.Domain.Repositories
{
    public class UsersRepository: RepositoryBase<User>, IUsersRepository
    {
        public UsersRepository(LibraryContext context) :
            base(context)
        {
        }
    }
}