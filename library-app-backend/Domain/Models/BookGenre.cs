using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace library_app_backend.Domain.Models
{
    [Table("genres")]
    public class BookGenre
    {
        private int _id;
        private string _name;
        private string _description;
        private ICollection<Book> _books;

        public BookGenre() 
        {
            _books = new HashSet<Book>();
        }

        [Column("id")]
        public int ID 
        {
            get { return _id; }
            set { _id = value; }
        }

        [Required]
        [Column("name")]
        [MaxLength(60)]
        public string Name 
        {
            get { return _name; }
            set { _name = value; }
        }

        [Required]
        [Column("description")]
        [MaxLength(256)]
        public string Description 
        {
            get { return _description; }
            set { _description = value; }
        }

        public ICollection<Book> Books 
        {
            get { return _books; }
            set { _books = value; }
        } 
    }
}