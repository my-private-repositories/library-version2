using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace library_app_backend.Domain.Models
{
    [Table("books")]
    public class Book
    {
        private int _id;
        private string _title;
        private string _description;
        private decimal _price;
        private ICollection<Author> _authors; 
        private ICollection<BookGenre> _bookGenres; 

        public Book() 
        {
            _authors = new HashSet<Author>();
            _bookGenres = new HashSet<BookGenre>();
        }

        [Column("id")]
        public int ID 
        {
            get { return _id; }
            set { _id = value; }
        }

        [Required]
        [Column("title")]
        [MaxLength(100)]
        public string Title 
        {
            get { return _title; }
            set { _title = value; }
        }

        [Required]
        [Column("description")]
        [MaxLength(256)]
        public string Description 
        {
            get { return _description; }
            set { _description = value; }
        }

        [Required]
        [Column("price")]
        public decimal Price 
        {
            get { return _price; }
            set { _price = value; }
        }

        public ICollection<Author> Authors
        {
            get { return _authors; }
            set { _authors = value; }
        } 

        public ICollection<BookGenre> BookGenres
        {
            get { return _bookGenres; }
            set { _bookGenres = value; }
        } 
    }
}