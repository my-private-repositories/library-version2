using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace library_app_backend.Domain.Models
{
    [Table("authors")]
    public class Author
    {
        private int _id;
        private string _firstName;
        private string _lastName;
        private string _description;
        private ICollection<Book> _books;

        public Author()
        {
            _books = new HashSet<Book>();
        }

        [Column("id")]
        public int ID 
        {
            get { return _id; }
            set { _id = value; }
        }

        [Required]
        [Column("first_name")]
        [MaxLength(60)]
        public string FirstName 
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        [Required]
        [Column("last_name")]
        [MaxLength(60)]
        public string LastName 
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        [Required]
        [Column("description")]
        [MaxLength(256)]
        public string Description 
        {
            get { return _description; }
            set { _description = value; }
        }

        public ICollection<Book> Books 
        {
            get { return _books; }
            set { _books = value; }
        } 
    }
}