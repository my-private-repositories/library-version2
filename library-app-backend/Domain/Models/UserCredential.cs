using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace library_app_backend.Domain.Models
{
    public enum UserRole 
    {
        ADMIN,
        USER
    }

    [Table("users_credentials")]
    public class UserCredential 
    {
        private string _userName;
        private string _passPhrase;
        private UserRole _userRole;

        [Required]
        [Column("user_name")]
        [MaxLength(60)]
        public string UserName { 
            get { return _userName; } 
            set {_userName = value; } 
        }

        [Required]
        [Column("pass_phrase")]
	    public string PassPhrase { 
            get { return _passPhrase; } 
            set {_passPhrase = value; } 
        }

        [Required]
        [Column("user_role")]
	    public UserRole UserRole { 
            get { return _userRole; } 
            set {_userRole = value; } 
        }

        [Column("user_id")]
        public int UserID {get; set;}
    }
}