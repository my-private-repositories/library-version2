using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace library_app_backend.Domain.Models
{
    [Table("users")]
    public class User 
    {
        private int _id;
        private string _firstName;
        private string _lastName;
        private string _address;
        private string _email;
        private UserCredential _credential;

        [Column("id")]
        public int ID { 
            get { return _id; } 
            set { _id = value; } 
        }

        [Required]
        [Column("first_name")]
        [MaxLength(60)]
          public string FirstName {
             get { return _firstName; } 
             set { _firstName = value; } 
          }

        [Required]
        [Column("last_name")]
        [MaxLength(60)]
	    public string LastName { 
             get { return _lastName; } 
             set { _lastName = value; } 
          }

        [Required]
        [Column("address")]
        [MaxLength(256)]
	    public string Address {
             get { return _address; } 
             set { _address = value; } 
          }

        [Required]
        [Column("email")]
	     public string Email { 
             get { return _email; } 
             set { _email = value; } 
          }
        
          public UserCredential Credential {
             get { return _credential; } 
             set { _credential = value; } 
          }
    }
}