﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace library_app_backend.Migrations
{
    public partial class CreatingUniqueIndexes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_users_credentials_user_name",
                table: "users_credentials",
                column: "user_name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_users_email",
                table: "users",
                column: "email",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_users_credentials_user_name",
                table: "users_credentials");

            migrationBuilder.DropIndex(
                name: "IX_users_email",
                table: "users");
        }
    }
}
