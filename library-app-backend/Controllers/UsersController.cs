using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Connections;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

using library_app_backend.DTO.Models;
using library_app_backend.DTO.Services;
using library_app_backend.DTO.Validators;
using library_app_backend.Domain.Repositories;

// using exceptions = library_app_backend.DTO.Validators.Exceptions; 

namespace library_app_backend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase 
    {
        private UsersService _service;

        public UsersController(IRepositoryWrapper wrapper) 
        {
            _service = new UsersService(wrapper);
        }

        [HttpGet]
        public ActionResult List() 
        {
            return Ok(_service.GetAll());
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult<User> Add([FromBody] User user)
        {
            try
            {
                if(!ModelState.IsValid)
                    return BadRequest(ModelState);

                User newU = _service.Create(user);

                return CreatedAtAction("Add", newU);
            }
            catch (Exception e)
            {
                ObjectResult r = new ObjectResult("teste");
                r.StatusCode=500;

                Console.WriteLine("Exc: {0} -> ", e);

                return r;
            }
        }
    }
}