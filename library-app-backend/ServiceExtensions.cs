using Microsoft.Extensions.DependencyInjection;

using library_app_backend.Domain.Repositories;

namespace library_app_backend
{
    public static class ServiceExtensions
    {
        public static void ConfigureRepositoryWrapper(this IServiceCollection services)
        {
            services.AddScoped<IRepositoryWrapper, RepositoryWrapper>();
        }
    }
}