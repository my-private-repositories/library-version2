using System;

namespace library_app_backend.DTO.Validators.Annotations
{
    public sealed class Email : ModelAttribute
    { 
        private const string _handler="Email";

        public Email() :
            base()
        {
            HandlerName = HandlerName + _handler;
        }

        // public string HandlerName 
        // {
        //     get { return _handlersNamespace + _handler; }
        // }

        public override Result IsValid(object value)
        {
            // if(value == null)
            //     return ValidationResult.Success;

            // string email = value as string;

            // Result res = Email.Create(email);

            // if(res.GetError != null)
            //     return new ValidationResult(res.GetError.Message);

             return new Result();
        }
    }
}
