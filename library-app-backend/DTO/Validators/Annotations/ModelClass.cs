using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

using library_app_backend.Domain.Repositories;

namespace library_app_backend.DTO.Validators.Annotations
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class ModelClassAttribute : ValidationAttribute
    {
        private IRepositoryWrapper _wrapper;
        private ModelAttributeChecker _checker = new ModelAttributeChecker();

        private void FillChecker(ValidationContext context)
        {
            _checker.Clean();

            Type t = context.ObjectType;

            foreach (var p in t.GetProperties())
            {
                 foreach (var a in p.GetCustomAttributes())
                    _checker.AddAttribute((ModelAttribute)a, p.Name, p.GetValue(context.ObjectInstance));
            }
        }

        protected override ValidationResult IsValid(object  value, ValidationContext context)
        {
            FillChecker(context);

            ErrorsList errors = _checker.GetErrors();

            if(errors.Count > 0)
            {
                List<string> fields = new List<string>();

                foreach (var item in errors)
                    fields.Add(item.Field);

                return new ValidationResult(errors[0].Message, fields);                
            }

            return ValidationResult.Success;
        }
    }
}
