using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

using handler = library_app_backend.DTO.Validators.Handlers;
using exceptions = library_app_backend.DTO.Validators.Exceptions;

namespace library_app_backend.DTO.Validators.Annotations
{
    internal class AttributeCheck
    {
        public string Name { get; set; }
        public object Handler { get; set; }
    }

    public class ModelAttributeChecker
    {
        private Hashtable _attributes;

        public ModelAttributeChecker()
        {
            _attributes = new Hashtable();
        }    

        public void Clean()
        {
            _attributes.Clear();
        }

        public void AddAttribute(ModelAttribute attr, string attrName, object value)
        {
            object handler = null;
            string handlerName = attr.HandlerName;

            if(!_attributes.ContainsKey(handlerName))
                _attributes[handlerName] = new List<AttributeCheck>();
                
            handler = GetHandler(attr, value);

            ((List<AttributeCheck>)_attributes[handlerName])
                .Add(new AttributeCheck{Name=attrName, Handler=handler});
        }

        private object GetHandler(ModelAttribute attr, object value) 
        {
            Type handlerType = Type.GetType(attr.HandlerName);

            if(handlerType == null)
            {   
                string msg = $"Impossible to validate attribute.";
                throw new exceptions.InvalidHandlerException { Value=msg };
            }
            
            Type valueType = value.GetType();
            Type[] types = { valueType };

            ConstructorInfo handlerConstructor = handlerType.GetConstructor(
                BindingFlags.Instance | BindingFlags.Public, null,
                CallingConventions.HasThis, types, null);

            if(handlerConstructor == null)
            {
                string msg = "Bad notation applied. The notation can not be applied to this kind of attribute.";
                throw new exceptions.InvalidHandlerException { Value=msg };
            }

            object[] args = { value };

            object handler = handlerConstructor.Invoke(args);

            if(handler == null)
            {
                string msg = $"Fail to create checker.";
                throw new exceptions.InvalidHandlerException { Value=msg };
            }

            return handler;
        }

        public ErrorsList GetErrors() 
        {
            ErrorsList errors = new ErrorsList();
            foreach (DictionaryEntry de in _attributes)
            {
                List<AttributeCheck> checks = (List<AttributeCheck>)de.Value;                
                
                foreach (var check in checks)
                {
                    string name = check.Name;
                    object handler = check.Handler;

                    MethodInfo checkError = handler.GetType().GetMethod("CheckError");

                    Result r = (Result) checkError.Invoke(handler, null);

                    if(r.GetError != null)
                        errors.Add(new Error{ Field=name, Message=r.GetError.Message });
                }
            }

            return errors;
        }
    }
}