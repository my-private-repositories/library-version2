using System;

namespace library_app_backend.DTO.Validators.Annotations
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple=true)]
    public abstract class ModelAttribute: Attribute
    {
        private const string _handlersNamespace="library_app_backend.DTO.Validators.Handlers.";

        public ModelAttribute()
        {
            HandlerName = _handlersNamespace;
        }
        
        public string HandlerName
        {
            get; set; 
        } 

        public abstract Result IsValid(object value);
    }
}