using System.Collections.Generic;

namespace library_app_backend.DTO.Validators
{
    public class Error
    {
        public string Field { get; set; }
        public string Message { get; set; }
    }

    public class ErrorsList : List<Error> {}
}