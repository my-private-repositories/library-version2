using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace library_app_backend.DTO.Validators.Handlers
{
    public class Email : IAttributeCheck<string>
    {
        public Email(string value) :
            base(value)
        {
        }

        public override Result CheckError()
        {
            if(_value == null)
                return Result.Ok();

            string email = _value.Trim();

            if (!Regex.IsMatch(email, @"^(.+)@(.+)$"))
                return Result.Failure(Errors.EmailInvalid());

            return Result.Ok();
        }
    }
}