using System;
using System.Collections.Generic;
using System.Reflection;

#nullable enable

namespace library_app_backend.DTO.Validators
{
    public abstract class IAttributeCheck<T>
    {
        protected T? _value;
        private static List<Result> _errors = new List<Result>();

        public IAttributeCheck(T? value) 
        {
            _value = value;
        }

        // protected void AddError(Result err)
        // {
        //     var hasError = err.GetError;

        //     if(hasError != null)
        //         _errors.Add(err);
        // }

        // public IEnumerable<Result> GetErrors() 
        // {
        //     AddError(CheckError());

        //     foreach(Result er in _errors)
        //         yield return er;
        // }

        public abstract Result CheckError();
    }

    // public class AttributeCheckDecorator<IC, T> : IAttributeCheck<T>
    //     where IC : IAttributeCheck<T>
    // {
    //     private IAttributeCheck<T> _decorated;

    //     public AttributeCheckDecorator(IC decorated, T value) :
    //         base(value)
    //     {
    //         _decorated = decorated;            
    //     }

    //     public override Result CheckError()
    //     {
    //         // Binder defaultBinder = GetType().DefaultBinder;

    //         // myClass.GetType().InvokeMember("CheckError", BindingFlags.InvokeMethod,
    //         //     defaultBinder, this, null);          
    //         // Type baseType = _decorated.GetType().BaseType;

    //          Console.WriteLine("IsSubclass: {0}", GetType().AssemblyQualifiedName);

    //         // MethodInfo checkError = baseType.GetMethod("CheckError");
    //         // Result r = (Result) checkError.Invoke(this, null);

    //         // Console.WriteLine("Dec: {0}", _decorated.GetType());
    //         // Result r = null;

    //         // if(r.GetError != null)
    //         //     AddError(r);

    //         // Result decError = _decorated.CheckError();

    //         // if(decError.GetError != null)
    //         //     AddError(decError);
            
    //         return Result.Ok();
    //     }
    // }
}