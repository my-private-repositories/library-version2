namespace library_app_backend.DTO.Validators
{
    public static class Errors 
    {
        public static ValidateError EmptyFieldViolation() =>
            new ValidateError(/*"empty.field.violation", */"The field must not be empty.");

        public static ValidateError EmailBeingUsed(string email) =>
            new ValidateError(/*"email.used", */"The email is being used. Try another one.");

        public static ValidateError EmailInvalid() =>
            new ValidateError(/*"email.invalid", */"The email format seems to be invalid.");

        public static ValidateError StringSizeViolation(int max) =>
            new ValidateError(/*"size.constraint.violation", */$"The field must have at most '{max}' characters.");

        public static ValidateError UserRoleInvalid() =>
            new ValidateError(/*"size.constraint.violation", */"Invalid value. Allowed values are 'ADMIN' or 'USER'.");

        public static ValidateError UniqueViolation() =>
            new ValidateError(/*"size.constraint.violation", */"Duplicated value! Try another one.");
    }
}