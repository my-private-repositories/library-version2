using System;
using System.Collections.Generic;

namespace library_app_backend.DTO.Validators
{
    public sealed class ValidateError : ValueObject
    {        
        public string Message { get; }

        internal ValidateError(string message)
        {
            Message = message;
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Message;
        }
    }
}