using System;

namespace library_app_backend.DTO.Validators.Exceptions
{
    public class InvalidHandlerException : Exception
    {
        public int Status { get; set; } = 500;

        public object Value { get; set; }
    }
}
