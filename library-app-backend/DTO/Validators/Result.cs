#nullable enable

namespace library_app_backend.DTO.Validators
{
    public class Result
    {
        private ValidateError? _error;

        public static Result Failure(ValidateError er)
        {
            Result res = new Result();
            res._error = er;

            return res;
        } 

        public static Result Ok()
        {
            return new Result();
        }

        public ValidateError? GetError 
        {
            get { return _error; }
        }
    }
}