using library_app_backend.DTO.Validators.Annotations;

#nullable enable

namespace library_app_backend.DTO.Models
{   
    [ModelClass]
    public class User 
    {
        public int? ID { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Address { get; set; }

        // [Required, Email, Unique]
        [Email]
        public string? Email { get; set; }

        // [Required, Unique]
        public string? UserName { get; set; }

        // [Required]
        public string? PassPhrase { get; set; }

        // [Required, UserRole]
        public string? UserRole { get; set; }
    }
}