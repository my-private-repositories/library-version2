using System.Collections.Generic;
using System.Linq;

using entity = library_app_backend.Domain.Models;
using library_app_backend.Domain.Repositories;
using library_app_backend.DTO.Converters;
using dto = library_app_backend.DTO.Models;

namespace library_app_backend.DTO.Services
{
    public class UsersService 
    {
        private IRepositoryWrapper _wrapper;
        private UserConverter _converter;

        public UsersService(IRepositoryWrapper wrapper)
        {
            _wrapper = wrapper;
            _converter = new UserConverter();
        }

        public dto.User Create(dto.User user) 
        {
            var newU = _converter.ToEntity(user);
            newU = _wrapper.UsersRepository.Create(newU);
            _wrapper.Save();

            return _converter.ToDTO(newU);
        }

        public IEnumerable<dto.User> GetAll()
        {
            var dtos = _converter.ToDTOCollection(_wrapper.UsersRepository.FindAll().ToList());

            return dtos;
        }
    }
}