using System;
using System.Collections.Generic;

using entity = library_app_backend.Domain.Models;
using dto = library_app_backend.DTO.Models;

namespace library_app_backend.DTO.Converters
{
    public class UserConverter : Converter<dto.User, entity.User>
    {
        public override entity.User ToEntity(dto.User u) 
        {
            var cred = new entity.UserCredential {
                UserName=u.UserName,
                PassPhrase=u.PassPhrase,
                UserRole=Enum.Parse<entity.UserRole>(u.UserRole)
            }; 

            var user = new entity.User {
                FirstName=u.FirstName,
                LastName=u.LastName,
                Address=u.Address,
                Email=u.Email,
                Credential=cred
            }; 

            return user;
        }

        public override dto.User ToDTO(entity.User u) 
        {
            var dU = new dto.User
            {
                ID=u.ID,
                FirstName=u.FirstName,
                LastName=u.LastName,
                Address=u.Address,
                Email=u.Email,
                UserName=u.Credential.UserName,
                UserRole=u.Credential.UserRole.ToString()
            };

            return dU;
        }
    }
}