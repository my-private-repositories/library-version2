using System.Collections.Generic;

namespace library_app_backend.DTO.Converters
{
    public abstract class Converter<D, E>
    {
        public abstract E ToEntity(D dto);
        public abstract D ToDTO(E entity);

        public IEnumerable<E> ToEntityCollection(IEnumerable<D> dtoList) 
        {
            foreach(D dto in dtoList)
                yield return ToEntity(dto);
        }

        public IEnumerable<D> ToDTOCollection(IEnumerable<E> entityList)
        {
            foreach(E entity in entityList)
                yield return ToDTO(entity);            
        }
    }
}