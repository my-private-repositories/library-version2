#!/bin/bash

openssl rand -out KEY -base64 32
openssl rand -out .docker/postgresql/root_password -base64 20
openssl rand -out .docker/postgresql/admin_password -base64 20

DB_FILE=.docker/postgresql/init_db.sql

KEY=$(cat < KEY)
ROOT_PASSWORD=$(cat < .docker/postgresql/root_password)
ADMIN_PASSWORD=$(cat < .docker/postgresql/admin_password)

./generate-db.sh $DB_FILE $ROOT_PASSWORD $ADMIN_PASSWORD